<?php

$randomArray = range(-25,25);
shuffle($randomArray);

function compareFunc (int $val1, int $val2): int {
    if ($val1 === $val2) {
        return 0;
    }
    return ($val1 > $val2) ? -1 : 1;
}

sort($randomArray);
shuffle($randomArray);
usort($randomArray, 'compareFunc');
shuffle($randomArray);

$exception = 18; // $exception = $this->getException();

usort($randomArray, static function (int $val1, int $val2) use ($exception) {
    if ($val1 === $exception) {
        return -1;
    }
    if ($val2 === $exception) {
        return 1;
    }
    if ($val1 === $val2) {
        return 0;
    }
    return ($val1 > $val2) ? -1 : 1;
});
shuffle($randomArray);
