<?php

$users = [
    0 => [
        'group' => [
            'name' => 'group name',
            'id' => 1,
        ],
        'name' => 'name1',
        'id' => 10,
        'age' => 38,
    ],
    1 => [
        'group' => [
            'name' => '', //is_set => true
            'id' => 1,
        ],
        'name' => 'name2',
        'id' => 11,
        'age' => 31,
    ],
    2 => [
        'name' => 'name3',
        'id' => 12,
        'age' => 31,
    ],
];

$groupName = $users[0]['group']['name'] ?: 'default1';
$groupName = $users[0]['group']['name'] ?? 'default2';

$groupName = $users[1]['group']['name'] ?: 'default3';
$groupName = $users[1]['group']['name'] ?? 'default4';

$groupName = $users[2]['group']['name'] ?: 'default5'; //Notice: Undefined index: group
$groupName = $users[2]['group']['name'] ?? 'default6';

$users[2]['group']['name'] ??= 'default';

echo die();