<?php

declare(strict_types=1);


$string1 = '';
$string2 = 'a';
$string3 = '1';
$string4 = '0';

echo 'String1 is ' . ($string1 ? 'true' : 'false') . PHP_EOL;
echo 'String2 is ' . ($string2 ? 'true' : 'false') . PHP_EOL;
echo 'String3 is ' . ($string3 ? 'true' : 'false') . PHP_EOL;
echo 'String4 is ' . ($string4 ? 'true' : 'false') . PHP_EOL;
echo PHP_EOL;


$array1 = [];
$array2 = [false];

echo 'Array1 is ' . ($array1 ? 'true' : 'false') . PHP_EOL;
echo 'Array2 is ' . ($array2 ? 'true' : 'false') . PHP_EOL;
echo PHP_EOL;

$int1 = 0;
$int2 = 1;

echo 'Int1 is ' . ($int1 ? 'true' : 'false') . PHP_EOL;
echo 'Int2 is ' . ($int2 ? 'true' : 'false') . PHP_EOL;
echo PHP_EOL;

$float1 = 0.0;
$float2 = 1.0;

echo 'Float1 is ' . ($float1 ? 'true' : 'false') . PHP_EOL;
echo 'Float2 is ' . ($float2 ? 'true' : 'false') . PHP_EOL;
echo PHP_EOL;

$object = new stdClass();

echo 'Object is ' . ($object ? 'true' : 'false') . PHP_EOL;
echo PHP_EOL;

$null = null;

echo 'Null is ' . ($null ? 'true' : 'false') . PHP_EOL;
echo PHP_EOL;

$resource = fopen(__DIR__ . '/1.txt', 'rb');

echo 'Resource is ' . ($resource ? 'true' : 'false') . PHP_EOL;
echo PHP_EOL;



$a = new stdClass();
$a->value = 10;
echo 'variable a = {$a->value}\n' . PHP_EOL;
echo "variable a = {$a->value}\n" . PHP_EOL;
