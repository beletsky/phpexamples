<?php

declare(strict_types=1);

$string = '';

echo gettype($string) . PHP_EOL;

$bool = true;

echo gettype($bool) . PHP_EOL;

$array = [];

echo gettype($array) . PHP_EOL;

$int = 0;

echo gettype($int) . PHP_EOL;

$double = 0.0;

echo gettype($double) . PHP_EOL;

$object = new stdClass();

echo gettype($object) . PHP_EOL;

$null = null;

echo gettype($null) . PHP_EOL;

$resource = fopen(__DIR__ . '/1.txt', 'rb');

echo gettype($resource) . PHP_EOL;



$array = [1,3 => 4,'a' => 5];
$serializedArray = serialize($array);
var_dump($serializedArray);

$int = 3;
$serializedInt = serialize($int);
var_dump($serializedInt);

$object->name = 'Anton';
$object->surname = 'Bialetski';
$serializedObject = serialize($object);
var_dump($serializedObject);

$serializedResource = serialize($resource);
var_dump($serializedResource);

$unserializedArray = unserialize($serializedArray);
$unserializedInt   = unserialize($serializedInt);
$unserializedObject = unserialize($serializedObject);
$unserializedResource = unserialize($serializedResource);

echo 'Fin';