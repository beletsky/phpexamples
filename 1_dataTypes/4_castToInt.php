<?php

declare(strict_types=1);

$string1 = 'a123';
$string2 = '123a123';

echo (int) $string1 . PHP_EOL;
echo (int) $string2 . PHP_EOL;

$array = [1, 2, '123', false];
echo (in_array(0, $array) ? 'Found' : 'Not found') . PHP_EOL;
echo (in_array(0, $array, true) ? 'Found' : 'Not found') . PHP_EOL;

if (1 == '1') {
    echo __LINE__;
}

if (1 === '1') {
    echo __LINE__;
}
