<?php

$a = [];

$a[] = 'a';
$a[3] = 'b';
$a['key'] ='c';
$a['array'] = [];
$a['array']['key'] = 'd';

$b = [
    'a',
    3 => 'b',
    'key' => 'c',
    'array' => [
        'key' => 'd',
    ],
];

var_dump($a);
var_dump($b);

foreach ($a as $key => $value) {
    echo 'Key = ' . $key . ' Value = ' . var_export($value, true) . PHP_EOL;
}


//SELECT * FROM user WHERE ISNULL(group_id);

$users = [
    [
        'group_id' => null,
        'name' => 'name1',
        'id' => 10,
        'age' => 38,
    ],
    [
        'group_id' => null,
        'name' => 'name2',
        'id' => 11,
        'age' => 31,
    ],
    [
        'group_id' => null,
        'name' => 'name2',
        'id' => 100,
        'age' => 29,
    ],
    [
        'group_id' => null,
        'name' => 'name3',
        'id' => 120,
        'age' => 28,
    ],
];


$usersReverse = array_reverse($users);
$usersOlderThan30 = array_filter($users, static fn(array $user) => $user['age'] > 30);
$ids = array_column($users, 'id');
$users = array_combine($ids, $users);

die();