<?php

require_once ('foo.php');

session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Session test</title>
</head>
<body>
<?php
$random = random_int(0, PHP_INT_MAX);
echo 'Current :' . $random . PHP_EOL; ?>
<br>
<?php
echo 'Prev : ' . ($_SESSION['prev'] ?? 'No prev');
$_SESSION['prev'] = $random;
?>
</body>
</html>