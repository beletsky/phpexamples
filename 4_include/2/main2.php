<?php

require_once ('ClassTest1.php');
require_once ('ClassTest2.php');

use \namespace1\ClassTest as ClassTest1;
use \namespace2\ClassTest as ClassTest2;

$a = new ClassTest1();
$b = new ClassTest2();

echo $a->test() . PHP_EOL;
echo $b->test() . PHP_EOL;