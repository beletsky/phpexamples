<?php

spl_autoload_register(static function (string $className): void {
    $namespaceNumber = substr($className, strlen('namespace'), 1);
    $className = substr($className, strlen('namespaceD/')) . $namespaceNumber . '.php';

    require_once $className;
});

use \namespace2\ClassTest as ClassTest2;
use \namespace1\ClassTest as ClassTest1;

$a = new ClassTest1();
$b = new ClassTest2();

echo $a->test() . PHP_EOL;
echo $b->test() . PHP_EOL;