<?php

function thirdPower(int& $x): void
{
    $x **= 3;
}